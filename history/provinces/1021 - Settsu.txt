# No previous file for Settsu
owner = G03
controller = G03
add_core = G03
culture = boek
religion = totemism
capital = ""

hre = no

base_tax = 1
base_production = 1
base_manpower = 1

trade_goods = unknown

native_size = 4
native_ferocity = 3
native_hostileness = 3