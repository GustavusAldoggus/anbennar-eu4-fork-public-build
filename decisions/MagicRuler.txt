country_decisions = {

	cast_siege_magic = {
	
		major = yes
		
		potential = {
			ruler_has_personality = mage_personality
		}
		allow = {
			any_province = {	#does work
				#limit = {
					sieged_by = ROOT
					unit_has_leader = yes
					fort_level = 1
					num_of_units_in_province = {
						who = ROOT
						amount = 1
					}
				#}
			}
			NOT = {
				has_ruler_modifier = ruler_recently_casted_spell
			}
			NOT = { has_ruler_flag = magic_menu_opened }
			is_lesser_in_union = no
		}
		effect = {
			country_event = { id = magic_siege.1 days = 0 }
			open_single_menu = yes	#prevents people from opening the menu multiple times
		}
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0
				OR = {
					NOT = { adm_power = 20 }
					NOT = { dip_power = 20 }
					NOT = { mil_power = 20 }
				}
			}
		}
	}
	
	
	# cast_battle_magic = {
	
		# major = yes
		
		# potential = {
			# #ruler_has_personality = mage_personality #disabling this cos it doesnt work
		# }
		# allow = {
			# any_province = {	#does work
				# #limit = {
					# unit_has_leader = yes
					# unit_in_battle = yes
					# num_of_units_in_province = {
						# who = ROOT
						# amount = 1
					# }
				# #}
			# }
			# NOT = {
				# has_ruler_modifier = ruler_recently_casted_spell
			# }
		# }
		# effect = {
			# country_event = { id = magic_battle.1 days = 0 }
		# }
		# ai_will_do = {
			# factor = 1
			# modifier = {
				# factor = 0
				# OR = {
					# NOT = { adm_power = 20 }
					# NOT = { dip_power = 20 }
					# NOT = { mil_power = 20 }
				# }
			# }
		# }
	# }
	
	
	cast_realm_magic = {
	
		major = yes
		
		potential = {
			ruler_has_personality = mage_personality
		}
		allow = {
			NOT = {
				has_ruler_modifier = ruler_recently_casted_spell
			}
			NOT = { has_ruler_flag = magic_menu_opened }
			is_lesser_in_union = no
		}
		effect = {
			country_event = { id = magic_realm.0 days = 0 }
			open_single_menu = yes	#prevents people from opening the menu multiple times
		}
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0
				OR = {
					NOT = { adm_power = 100 }
					NOT = { dip_power = 100 }
					NOT = { mil_power = 100 }
				}
			}
			modifier = {
				factor = 0
				NOT = { has_ruler_flag = abjuration_1 }
				NOT = { has_ruler_flag = conjuration_1 }
				NOT = { has_ruler_flag = divination_1 }
				NOT = { has_ruler_flag = illusion_1 }
				NOT = { has_ruler_flag = enchantment_1 }
				NOT = { has_ruler_flag = evocation_1 }
				NOT = { has_ruler_flag = necromancy_1 }
				NOT = { has_ruler_flag = transmutation_1 }
			}
		}
	}
	
	
	study_magic = {
	
		major = yes
		
		potential = {
			ruler_has_personality = mage_personality
		}
		allow = {
				NOT = { has_ruler_modifier = ruler_studying_magic }
				NOT = { has_ruler_flag = ruler_studying_abjuration } 
				NOT = { has_ruler_flag = ruler_studying_conjuration } 
				NOT = { has_ruler_flag = ruler_studying_divination } 
				NOT = { has_ruler_flag = ruler_studying_enchantment } 
				NOT = { has_ruler_flag = ruler_studying_evocation } 
				NOT = { has_ruler_flag = ruler_studying_illusion } 
				NOT = { has_ruler_flag = ruler_studying_necromancy } 
				NOT = { has_ruler_flag = ruler_studying_transmutation }
				
				NOT = { has_ruler_flag = magic_menu_opened }
				is_lesser_in_union = no
		}
		effect = {
			country_event = { id = magic_study.0 days = 0 }
			open_single_menu = yes	#prevents people from opening the menu multiple times
		}
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0
				OR = { 
					NOT = { adm_power = 50 }
					NOT = { dip_power = 50 }
					NOT = { mil_power = 50 }
				}
			}
		}
	}
	
}