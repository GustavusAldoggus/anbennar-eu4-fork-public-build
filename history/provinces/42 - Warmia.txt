#42 - Warmia |

owner = A11
controller = A11
add_core = A11
culture = pearlsedger
religion = regent_court

hre = yes

base_tax = 3
base_production = 2
base_manpower = 2

trade_goods = grain

capital = ""

is_city = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
