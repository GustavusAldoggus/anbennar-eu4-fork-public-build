#180 - Poitou | 

owner = A27
controller = A27
add_core = A27
culture = redscale_kobold
religion = kobold_dragon_cult

hre = no

base_tax = 2
base_production = 2
base_manpower = 2

trade_goods = iron

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_kobold

477.1.1 = {	
	owner = A27
	controller = A27
	add_core = A27
}
