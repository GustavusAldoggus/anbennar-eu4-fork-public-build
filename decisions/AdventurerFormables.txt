country_decisions = {

	luciande_nation = {
		major = yes
		potential = {
			normal_or_historical_nations = yes
			NOT = { has_country_flag = formed_luciande }
			NOT = { exists = B34 }
			
			tag = B11 
			
			OR = {
				ai = no
				is_playing_custom_nation = no
			}
			OR = {
				ai = no
				AND = {
					ai = yes
					num_of_cities = 3
				}
			}
			is_colonial_nation = no
			OR = {
				is_former_colonial_nation = no
				AND = {
					is_former_colonial_nation = yes
					ai = no
				}
			}
		}
		allow = {
			adm_tech = 7	#around 1500
			is_free_or_tributary_trigger = yes
			is_nomad = no
			is_at_war = no
			num_of_cities = 3
		}
		effect = {
			change_tag = B34
			remove_non_electors_emperors_from_empire_effect = yes

			add_prestige = 25
			add_country_modifier = {
				name = "legacy_of_adventurers_modifier"
				duration = 14600
			}
			set_country_flag = adventurer_derived_government
			set_country_flag = formed_luciande_flag
			clr_country_flag = 	knightly_order_adventurer
			country_event = { id = marcher.1 days = 1 }
			if = {
				limit = { has_custom_ideas = no }
				country_event = { id = ideagroups.1 } #Swap Ideas
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}
	
	ancardia_nation = {
		major = yes
		potential = {
			normal_or_historical_nations = yes
			NOT = { has_country_flag = formed_ancardia }
			NOT = { exists = B35 }
			
			tag = B06 
			
			OR = {
				ai = no
				is_playing_custom_nation = no
			}
			OR = {
				ai = no
				AND = {
					ai = yes
					num_of_cities = 3
				}
			}
			is_colonial_nation = no
			OR = {
				is_former_colonial_nation = no
				AND = {
					is_former_colonial_nation = yes
					ai = no
				}
			}
		}
		allow = {
			adm_tech = 7
			is_free_or_tributary_trigger = yes
			is_nomad = no
			is_at_war = no
			num_of_cities = 3
		}
		effect = {
			change_tag = B35
			remove_non_electors_emperors_from_empire_effect = yes

			add_prestige = 25
			add_country_modifier = {
				name = "legacy_of_adventurers_modifier"
				duration = 14600
			}
			set_country_flag = adventurer_derived_government
			set_country_flag = formed_ancardia_flag
			clr_country_flag = 	knightly_order_adventurer
			country_event = { id = marcher.1 days = 1 }
			if = {
				limit = { has_custom_ideas = no }
				country_event = { id = ideagroups.1 } #Swap Ideas
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}
	
	rogieria_nation = {
		major = yes
		potential = {
			normal_or_historical_nations = yes
			NOT = { has_country_flag = formed_rogieria }
			NOT = { exists = B37 }
			
			tag = B07
			
			OR = {
				ai = no
				is_playing_custom_nation = no
			}
			OR = {
				ai = no
				AND = {
					ai = yes
					num_of_cities = 3
				}
			}
			is_colonial_nation = no
			OR = {
				is_former_colonial_nation = no
				AND = {
					is_former_colonial_nation = yes
					ai = no
				}
			}
		}
		allow = {
			adm_tech = 7
			is_free_or_tributary_trigger = yes
			is_nomad = no
			is_at_war = no
			num_of_cities = 3
		}
		effect = {
			change_tag = B37
			remove_non_electors_emperors_from_empire_effect = yes

			add_prestige = 25
			add_country_modifier = {
				name = "legacy_of_adventurers_modifier"
				duration = 14600
			}
			set_country_flag = adventurer_derived_government
			set_country_flag = formed_rogieria_flag
			clr_country_flag = 	knightly_order_adventurer
			country_event = { id = marcher.1 days = 1 }
			if = {
				limit = { has_custom_ideas = no }
				country_event = { id = ideagroups.1 } #Swap Ideas
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}
	
	elikhand_nation = {
		major = yes
		potential = {
			normal_or_historical_nations = yes
			NOT = { has_country_flag = formed_elikhand }
			NOT = { exists = B38 }
			
			tag = B15
			
			OR = {
				ai = no
				is_playing_custom_nation = no
			}
			OR = {
				ai = no
				AND = {
					ai = yes
					num_of_cities = 3
				}
			}
			is_colonial_nation = no
			OR = {
				is_former_colonial_nation = no
				AND = {
					is_former_colonial_nation = yes
					ai = no
				}
			}
		}
		allow = {
			adm_tech = 7
			is_free_or_tributary_trigger = yes
			is_nomad = no
			is_at_war = no
			num_of_cities = 3
		}
		effect = {
			change_tag = B38
			remove_non_electors_emperors_from_empire_effect = yes

			add_prestige = 25
			add_country_modifier = {
				name = "legacy_of_adventurers_modifier"
				duration = 14600
			}
			set_country_flag = adventurer_derived_government
			set_country_flag = formed_elikhand_flag
			clr_country_flag = 	knightly_order_adventurer
			country_event = { id = marcher.1 days = 1 }
			if = {
				limit = { has_custom_ideas = no }
				country_event = { id = ideagroups.1 } #Swap Ideas
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}
	
	wyvernheart_nation = {
		major = yes
		potential = {
			normal_or_historical_nations = yes
			NOT = { has_country_flag = formed_wyvernheart }
			NOT = { exists = B39 }
			
			tag = B12
			
			OR = {
				ai = no
				is_playing_custom_nation = no
			}
			OR = {
				ai = no
				AND = {
					ai = yes
					num_of_cities = 3
				}
			}
			is_colonial_nation = no
			OR = {
				is_former_colonial_nation = no
				AND = {
					is_former_colonial_nation = yes
					ai = no
				}
			}
		}
		allow = {
			adm_tech = 7
			is_free_or_tributary_trigger = yes
			is_nomad = no
			is_at_war = no
			num_of_cities = 3
		}
		effect = {
			change_tag = B39
			remove_non_electors_emperors_from_empire_effect = yes

			add_prestige = 25
			add_country_modifier = {
				name = "legacy_of_adventurers_modifier"
				duration = 14600
			}
			set_country_flag = adventurer_derived_government
			set_country_flag = formed_wyvernheart_flag
			clr_country_flag = 	knightly_order_adventurer
			country_event = { id = marcher.1 days = 1 }
			if = {
				limit = { has_custom_ideas = no }
				country_event = { id = ideagroups.1 } #Swap Ideas
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}
	
	alenor_nation = {
		major = yes
		potential = {
			normal_or_historical_nations = yes
			NOT = { has_country_flag = formed_alenor }
			NOT = { exists = B40 }
			
			tag = B03
			
			OR = {
				ai = no
				is_playing_custom_nation = no
			}
			OR = {
				ai = no
				AND = {
					ai = yes
					num_of_cities = 3
				}
			}
			is_colonial_nation = no
			OR = {
				is_former_colonial_nation = no
				AND = {
					is_former_colonial_nation = yes
					ai = no
				}
			}
		}
		allow = {
			adm_tech = 7
			is_free_or_tributary_trigger = yes
			is_nomad = no
			is_at_war = no
			num_of_cities = 3
		}
		effect = {
			change_tag = B40
			remove_non_electors_emperors_from_empire_effect = yes

			add_prestige = 25
			add_country_modifier = {
				name = "legacy_of_adventurers_modifier"
				duration = 14600
			}
			set_country_flag = adventurer_derived_government
			set_country_flag = formed_alenor_flag
			clr_country_flag = 	knightly_order_adventurer
			country_event = { id = marcher.1 days = 1 }
			if = {
				limit = { has_custom_ideas = no }
				country_event = { id = ideagroups.1 } #Swap Ideas
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}
	
	stalbor_nation = {
		major = yes
		potential = {
			normal_or_historical_nations = yes
			NOT = { has_country_flag = formed_stalbor }
			NOT = { exists = B47 }
			
			tag = B13
			
			OR = {
				ai = no
				is_playing_custom_nation = no
			}
			OR = {
				ai = no
				AND = {
					ai = yes
					num_of_cities = 3
				}
			}
			is_colonial_nation = no
			OR = {
				is_former_colonial_nation = no
				AND = {
					is_former_colonial_nation = yes
					ai = no
				}
			}
		}
		allow = {
			adm_tech = 7
			is_free_or_tributary_trigger = yes
			is_nomad = no
			is_at_war = no
			num_of_cities = 3
		}
		effect = {
			change_tag = B47
			remove_non_electors_emperors_from_empire_effect = yes

			add_prestige = 25
			add_country_modifier = {
				name = "legacy_of_adventurers_modifier"
				duration = 14600
			}
			set_country_flag = adventurer_derived_government
			set_country_flag = formed_stalbor_flag
			clr_country_flag = 	knightly_order_adventurer
			country_event = { id = marcher.1 days = 1 }
			if = {
				limit = { has_custom_ideas = no }
				country_event = { id = ideagroups.1 } #Swap Ideas
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}
	
	ravenmarch_nation = {
		major = yes
		potential = {
			normal_or_historical_nations = yes
			NOT = { has_country_flag = formed_ravenmarch }
			NOT = { exists = B48 }
			
			tag = B14
			
			OR = {
				ai = no
				is_playing_custom_nation = no
			}
			OR = {
				ai = no
				AND = {
					ai = yes
					num_of_cities = 3
				}
			}
			is_colonial_nation = no
			OR = {
				is_former_colonial_nation = no
				AND = {
					is_former_colonial_nation = yes
					ai = no
				}
			}
		}
		allow = {
			adm_tech = 7
			is_free_or_tributary_trigger = yes
			is_nomad = no
			is_at_war = no
			num_of_cities = 3
		}
		effect = {
			change_tag = B48
			remove_non_electors_emperors_from_empire_effect = yes

			add_prestige = 25
			add_country_modifier = {
				name = "legacy_of_adventurers_modifier"
				duration = 14600
			}
			set_country_flag = adventurer_derived_government
			set_country_flag = formed_ravenmarch_flag
			clr_country_flag = 	knightly_order_adventurer
			country_event = { id = marcher.1 days = 1 }
			if = {
				limit = { has_custom_ideas = no }
				country_event = { id = ideagroups.1 } #Swap Ideas
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}
	
	araionn_nation = {
		major = yes
		potential = {
			normal_or_historical_nations = yes
			NOT = { has_country_flag = formed_araionn }
			NOT = { exists = B49 }
			
			tag = B09
			
			OR = {
				ai = no
				is_playing_custom_nation = no
			}
			OR = {
				ai = no
				AND = {
					ai = yes
					num_of_cities = 3
				}
			}
			is_colonial_nation = no
			OR = {
				is_former_colonial_nation = no
				AND = {
					is_former_colonial_nation = yes
					ai = no
				}
			}
		}
		allow = {
			adm_tech = 7
			is_free_or_tributary_trigger = yes
			is_nomad = no
			is_at_war = no
			num_of_cities = 3
		}
		effect = {
			change_tag = B49
			remove_non_electors_emperors_from_empire_effect = yes

			add_prestige = 25
			add_country_modifier = {
				name = "legacy_of_adventurers_modifier"
				duration = 14600
			}
			set_country_flag = adventurer_derived_government
			set_country_flag = formed_araionn_flag
			clr_country_flag = 	knightly_order_adventurer
			country_event = { id = marcher.1 days = 1 }
			if = {
				limit = { has_custom_ideas = no }
				country_event = { id = ideagroups.1 } #Swap Ideas
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}
	
	newshire_nation = {
		major = yes
		potential = {
			normal_or_historical_nations = yes
			NOT = { has_country_flag = formed_newshire }
			NOT = { exists = B50 }
			
			tag = B10
			
			OR = {
				ai = no
				is_playing_custom_nation = no
			}
			OR = {
				ai = no
				AND = {
					ai = yes
					num_of_cities = 3
				}
			}
			is_colonial_nation = no
			OR = {
				is_former_colonial_nation = no
				AND = {
					is_former_colonial_nation = yes
					ai = no
				}
			}
		}
		allow = {
			adm_tech = 7
			is_free_or_tributary_trigger = yes
			is_nomad = no
			is_at_war = no
			num_of_cities = 3
		}
		effect = {
			change_tag = B50
			remove_non_electors_emperors_from_empire_effect = yes

			add_prestige = 25
			add_country_modifier = {
				name = "legacy_of_adventurers_modifier"
				duration = 14600
			}
			set_country_flag = adventurer_derived_government
			set_country_flag = formed_newshire_flag
			clr_country_flag = 	knightly_order_adventurer
			country_event = { id = marcher.1 days = 1 }
			if = {
				limit = { has_custom_ideas = no }
				country_event = { id = ideagroups.1 } #Swap Ideas
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}
	
	estaire_nation = {
		major = yes
		potential = {
			normal_or_historical_nations = yes
			NOT = { has_country_flag = formed_estaire }
			NOT = { exists = B51 }
			
			tag = B08
			
			OR = {
				ai = no
				is_playing_custom_nation = no
			}
			OR = {
				ai = no
				AND = {
					ai = yes
					num_of_cities = 3
				}
			}
			is_colonial_nation = no
			OR = {
				is_former_colonial_nation = no
				AND = {
					is_former_colonial_nation = yes
					ai = no
				}
			}
		}
		allow = {
			adm_tech = 7
			is_free_or_tributary_trigger = yes
			is_nomad = no
			is_at_war = no
			num_of_cities = 3
		}
		effect = {
			change_tag = B51
			remove_non_electors_emperors_from_empire_effect = yes

			add_prestige = 25
			add_country_modifier = {
				name = "legacy_of_adventurers_modifier"
				duration = 14600
			}
			set_country_flag = adventurer_derived_government
			set_country_flag = formed_estaire_flag
			clr_country_flag = 	knightly_order_adventurer
			country_event = { id = marcher.1 days = 1 }
			if = {
				limit = { has_custom_ideas = no }
				country_event = { id = ideagroups.1 } #Swap Ideas
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}
	
	anbenland_nation = {
		major = yes
		potential = {
			normal_or_historical_nations = yes
			NOT = { has_country_flag = formed_anbenland }
			NOT = { exists = B52 }
			
			tag = B16
			
			OR = {
				ai = no
				is_playing_custom_nation = no
			}
			OR = {
				ai = no
				AND = {
					ai = yes
					num_of_cities = 3
				}
			}
			is_colonial_nation = no
			OR = {
				is_former_colonial_nation = no
				AND = {
					is_former_colonial_nation = yes
					ai = no
				}
			}
		}
		allow = {
			adm_tech = 7
			is_free_or_tributary_trigger = yes
			is_nomad = no
			is_at_war = no
			num_of_cities = 3
		}
		effect = {
			change_tag = B52
			remove_non_electors_emperors_from_empire_effect = yes

			add_prestige = 25
			add_country_modifier = {
				name = "legacy_of_adventurers_modifier"
				duration = 14600
			}
			set_country_flag = adventurer_derived_government
			set_country_flag = formed_anbenland_flag
			clr_country_flag = 	knightly_order_adventurer
			country_event = { id = marcher.1 days = 1 }
			if = {
				limit = { has_custom_ideas = no }
				country_event = { id = ideagroups.1 } #Swap Ideas
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}
	
	nurcestir_nation = {
		major = yes
		potential = {
			normal_or_historical_nations = yes
			NOT = { has_country_flag = formed_nurcestir }
			NOT = { exists = B53 }
			
			tag = B05
			
			OR = {
				ai = no
				is_playing_custom_nation = no
			}
			OR = {
				ai = no
				AND = {
					ai = yes
					num_of_cities = 3
				}
			}
			is_colonial_nation = no
			OR = {
				is_former_colonial_nation = no
				AND = {
					is_former_colonial_nation = yes
					ai = no
				}
			}
		}
		allow = {
			adm_tech = 7
			is_free_or_tributary_trigger = yes
			is_nomad = no
			is_at_war = no
			num_of_cities = 3
		}
		effect = {
			change_tag = B53
			remove_non_electors_emperors_from_empire_effect = yes

			add_prestige = 25
			add_country_modifier = {
				name = "legacy_of_adventurers_modifier"
				duration = 14600
			}
			set_country_flag = adventurer_derived_government
			set_country_flag = formed_nurcestir_flag
			clr_country_flag = 	knightly_order_adventurer
			country_event = { id = marcher.1 days = 1 }
			if = {
				limit = { has_custom_ideas = no }
				country_event = { id = ideagroups.1 } #Swap Ideas
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}

	esthil_nation = {
		major = yes
		potential = {
			normal_or_historical_nations = yes
			NOT = { has_country_flag = formed_esthil }
			NOT = { exists = B54 }
			
			tag = B20
			
			OR = {
				ai = no
				is_playing_custom_nation = no
			}
			OR = {
				ai = no
				AND = {
					ai = yes
					num_of_cities = 3
				}
			}
			is_colonial_nation = no
			OR = {
				is_former_colonial_nation = no
				AND = {
					is_former_colonial_nation = yes
					ai = no
				}
			}
		}
		allow = {
			adm_tech = 7
			is_free_or_tributary_trigger = yes
			is_nomad = no
			is_at_war = no
			num_of_cities = 3
		}
		effect = {
			change_tag = B54
			remove_non_electors_emperors_from_empire_effect = yes

			add_prestige = 25
			add_country_modifier = {
				name = "legacy_of_adventurers_modifier"
				duration = 14600
			}
			set_country_flag = adventurer_derived_government
			set_country_flag = formed_esthil_flag
			clr_country_flag = 	knightly_order_adventurer
			country_event = { id = marcher.1 days = 1 }
			if = {
				limit = { has_custom_ideas = no }
				country_event = { id = ideagroups.1 } #Swap Ideas
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}
	
	rosande_nation = {
		major = yes
		potential = {
			normal_or_historical_nations = yes
			NOT = { has_country_flag = formed_esthil }
			NOT = { exists = Z35 }
			
			tag = B04
			
			OR = {
				ai = no
				is_playing_custom_nation = no
			}
			OR = {
				ai = no
				AND = {
					ai = yes
					num_of_cities = 3
				}
			}
			is_colonial_nation = no
			OR = {
				is_former_colonial_nation = no
				AND = {
					is_former_colonial_nation = yes
					ai = no
				}
			}
		}
		allow = {
			adm_tech = 7
			is_free_or_tributary_trigger = yes
			is_nomad = no
			is_at_war = no
			num_of_cities = 3
		}
		effect = {
			change_tag = Z35
			remove_non_electors_emperors_from_empire_effect = yes

			add_prestige = 25
			add_country_modifier = {
				name = "legacy_of_adventurers_modifier"
				duration = 14600
			}
			set_country_flag = adventurer_derived_government
			set_country_flag = formed_esthil_flag
			clr_country_flag = 	knightly_order_adventurer
			country_event = { id = marcher.1 days = 1 }
			if = {
				limit = { has_custom_ideas = no }
				country_event = { id = ideagroups.1 } #Swap Ideas
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}
	
	hammerhome_nation = {
		major = yes
		potential = {
			normal_or_historical_nations = yes
			NOT = { has_country_flag = formed_esthil }
			NOT = { exists = Z36 }
			
			tag = B18
			
			OR = {
				ai = no
				is_playing_custom_nation = no
			}
			OR = {
				ai = no
				AND = {
					ai = yes
					num_of_cities = 3
				}
			}
			is_colonial_nation = no
			OR = {
				is_former_colonial_nation = no
				AND = {
					is_former_colonial_nation = yes
					ai = no
				}
			}
		}
		allow = {
			adm_tech = 7
			is_free_or_tributary_trigger = yes
			is_nomad = no
			is_at_war = no
			num_of_cities = 3
		}
		effect = {
			change_tag = Z36
			remove_non_electors_emperors_from_empire_effect = yes

			add_prestige = 25
			add_country_modifier = {
				name = "legacy_of_adventurers_modifier"
				duration = 14600
			}
			set_country_flag = adventurer_derived_government
			set_country_flag = formed_hammerhome_flag
			clr_country_flag = 	knightly_order_adventurer
			country_event = { id = marcher.1 days = 1 }
			if = {
				limit = { has_custom_ideas = no }
				country_event = { id = ideagroups.1 } #Swap Ideas
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}
	
	covenblad_nation = {
		major = yes
		potential = {
			normal_or_historical_nations = yes
			NOT = { has_country_flag = formed_esthil }
			NOT = { exists = Z37 }
			
			tag = B19
			
			OR = {
				ai = no
				is_playing_custom_nation = no
			}
			OR = {
				ai = no
				AND = {
					ai = yes
					num_of_cities = 3
				}
			}
			is_colonial_nation = no
			OR = {
				is_former_colonial_nation = no
				AND = {
					is_former_colonial_nation = yes
					ai = no
				}
			}
		}
		allow = {
			adm_tech = 7
			is_free_or_tributary_trigger = yes
			is_nomad = no
			is_at_war = no
			num_of_cities = 3
		}
		effect = {
			change_tag = Z37
			remove_non_electors_emperors_from_empire_effect = yes

			add_prestige = 25
			add_country_modifier = {
				name = "legacy_of_adventurers_modifier"
				duration = 14600
			}
			set_country_flag = adventurer_derived_government
			set_country_flag = formed_covenblad_flag
			clr_country_flag = 	knightly_order_adventurer
			country_event = { id = marcher.1 days = 1 }
			if = {
				limit = { has_custom_ideas = no }
				country_event = { id = ideagroups.1 } #Swap Ideas
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}

	silvermere_nation = {
		major = yes
		potential = {
			normal_or_historical_nations = yes
			NOT = { has_country_flag = formed_silvermere }
			NOT = { exists = B56 }
			
			OR = {
				ai = no
				is_playing_custom_nation = no
			}
			OR = {
				ai = no
				AND = {
					ai = yes
					num_of_cities = 3
				}
			}
			is_colonial_nation = no
			has_reform = adventurer_reform
			owns_core_province = 756
			OR = {
				is_former_colonial_nation = no
				AND = {
					is_former_colonial_nation = yes
					ai = no
				}
			}
		}
		allow = {
			adm_tech = 7
			is_free_or_tributary_trigger = yes
			is_nomad = no
			is_at_war = no
			owns_core_province = 756
			num_of_owned_provinces_with = {
				value = 9
				OR = {
					area = silvervord_area
					area = nortmerewood_area
					area = oudmerewood_area
					area = oudeben_area
					area = themin_area
					area = burnoll_area
					area = southgate_area
					area = nortmere_area
				}
			}
		}
		effect = {
			change_tag = B56
			remove_non_electors_emperors_from_empire_effect = yes

			add_prestige = 25
			add_country_modifier = {
				name = "legacy_of_adventurers_modifier"
				duration = 14600
			}
			set_country_flag = adventurer_derived_government
			set_country_flag = formed_silvermere_flag
			clr_country_flag = 	knightly_order_adventurer
			country_event = { id = marcher.1 days = 1 }
			if = {
				limit = { has_custom_ideas = no }
				country_event = { id = ideagroups.1 } #Swap Ideas
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}
	
}