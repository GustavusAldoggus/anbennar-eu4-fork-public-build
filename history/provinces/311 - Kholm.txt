#311 - Kholm

owner = A60
controller = A60
add_core = A60
culture = arannese
religion = regent_court

hre = yes

base_tax = 5
base_production = 5
base_manpower = 3

trade_goods = grain

capital = ""

is_city = yes
fort_15th = yes 

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish