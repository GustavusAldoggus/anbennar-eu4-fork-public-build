monstrous_purge = {	#links to monstrous_purge events
	local_development_cost = 0.5
	local_state_maintenance_modifier = 1
	
	# 	local_culture_conversion_cost = -0.25 #would be cool for another thing or the proper system
}


#Racial Modifiers
#Divided into maluses and bonuses (each have 2 bonuses, at the smallest oppression it only has 1 of the two)

#Elves - when oppressed they don't contribute much but when integrated they help the economy greatly with their elvish crafts
elven_minority_oppressed_small = {
	local_state_maintenance_modifier = 0.10
	local_unrest = 2
	local_autonomy = 0.05
	
	local_tax_modifier = 0.05
	#local_production_efficiency = 0.05
}

elven_minority_oppressed_large = {
	local_state_maintenance_modifier = 0.15
	local_unrest = 3
	local_autonomy = 0.1
	
	local_tax_modifier = 0.05
	local_production_efficiency = 0.05
}

elven_minority_coexisting_small = {
	local_state_maintenance_modifier = 0.15
	local_unrest = 1
	local_autonomy = 0.05
	
	local_tax_modifier = 0.10
	local_production_efficiency = 0.05
}

elven_minority_coexisting_large = {
	local_state_maintenance_modifier = 0.20
	local_unrest = 2
	local_autonomy = 0.1
	
	local_tax_modifier = 0.10
	local_production_efficiency = 0.10
}

elven_minority_integrated_small = {
	local_state_maintenance_modifier = 0.20
	local_unrest = 0
	local_autonomy = 0.05
	
	local_tax_modifier = 0.15
	local_production_efficiency = 0.10
}

elven_minority_integrated_large = {
	local_state_maintenance_modifier = 0.25
	local_unrest = 1
	local_autonomy = 0.1
	
	local_tax_modifier = 0.15
	local_production_efficiency = 0.15
}

#Dwarves - when oppressed they're just builders used for manual labour, when integrated they can focus on more intricate dwarven works
dwarven_minority_oppressed_small = {
	local_state_maintenance_modifier = 0.10
	local_unrest = 2
	local_autonomy = 0.05
	
	local_build_time = -0.25				#Get them dwarven builders in!
	#local_production_efficiency = 0.05
}

dwarven_minority_oppressed_large = {
	local_state_maintenance_modifier = 0.20	#Dwarves are rowdy
	local_unrest = 3
	local_autonomy = 0.1
	
	local_build_time = -0.35
	local_production_efficiency = 0.05		#More dwarves allow some to focus on their craft in addition to manual labour
}

dwarven_minority_coexisting_small = {
	local_state_maintenance_modifier = 0.20
	local_unrest = 1
	local_autonomy = 0.05
	
	local_build_time = -0.25
	local_production_efficiency = 0.05
}
dwarven_minority_coexisting_large = {
	local_state_maintenance_modifier = 0.25
	local_unrest = 2
	local_autonomy = 0.1
	
	local_build_time = -0.35
	local_production_efficiency = 0.1
}

dwarven_minority_integrated_small = {
	local_state_maintenance_modifier = 0.25
	local_unrest = 0
	local_autonomy = 0.05
	
	local_build_time = -0.35
	local_production_efficiency = 0.15
}

dwarven_minority_integrated_large = {
	local_state_maintenance_modifier = 0.30
	local_unrest = 1
	local_autonomy = 0.1
	
	local_build_time = -0.5
	local_production_efficiency = 0.15
}

#Gnomes - when oppressed they offer meagre goods and trinkets, when integrated they offer a booming academic community
gnomish_minority_oppressed_small = {
	local_state_maintenance_modifier = 0.10
	local_unrest = 1
	local_autonomy = 0.05
	
	local_production_efficiency = 0.05			#Gnomes are clever tinkers and artisans
	#local_institution_spread = 0.05
}

gnomish_minority_oppressed_large = {
	local_state_maintenance_modifier = 0.15
	local_unrest = 2
	local_autonomy = 0.1
	
	local_production_efficiency = 0.05
	local_institution_spread = 0.05				#Intelligentsia community starts to form
}

gnomish_minority_coexisting_small = {
	local_state_maintenance_modifier = 0.15
	local_unrest = 1
	local_autonomy = 0.05
	
	local_production_efficiency = 0.1
	local_institution_spread = 0.05
}
gnomish_minority_coexisting_large = {
	local_state_maintenance_modifier = 0.20
	local_unrest = 2
	local_autonomy = 0.1
	
	local_production_efficiency = 0.1
	local_institution_spread = 0.1
}

gnomish_minority_integrated_small = {
	local_state_maintenance_modifier = 0.25		#Integrating gnomes makes it all confusing cos gnomes are confusing and babble all the time which makes administration really confusing like this
	local_unrest = 0
	local_autonomy = 0.05
	
	local_production_efficiency = 0.1
	local_institution_spread = 0.15
}

gnomish_minority_integrated_large = {
	local_state_maintenance_modifier = 0.30		#ohgodno
	local_unrest = 1
	local_autonomy = 0.1
	
	local_production_efficiency = 0.15
	local_institution_spread = 0.20				#Intellgentsia community booms
}

#Halflings - when oppressed they're good for growing provinces, when integrated they're super good at growing provinces + help a lot with trade
halfling_minority_oppressed_small = {
	local_state_maintenance_modifier = 0.10
	local_unrest = 1
	local_autonomy = 0.05
	
	local_development_cost = -0.1				#The abundance and versatility of halflings make them suited for any odd jobs that thrive in an urban setting
	#province_trade_power_modifier = 0.05
}

halfling_minority_oppressed_large = {
	local_state_maintenance_modifier = 0.20		#Halfling crime goes up
	local_unrest = 2
	local_autonomy = 0.1
	
	local_development_cost = -0.1
	province_trade_power_modifier = 0.05		#Halfling wares and stores pop-up
}

halfling_minority_coexisting_small = {
	local_state_maintenance_modifier = 0.15
	local_unrest = 1
	local_autonomy = 0.05
	
	local_development_cost = -0.1
	province_trade_power_modifier = 0.1
}

halfling_minority_coexisting_large = {
	local_state_maintenance_modifier = 0.25
	local_unrest = 2
	local_autonomy = 0.1
	
	local_development_cost = -0.15
	province_trade_power_modifier = 0.05
}

halfling_minority_integrated_small = {
	local_state_maintenance_modifier = 0.2
	local_unrest = 0
	local_autonomy = 0.05
	
	local_development_cost = -0.15
	province_trade_power_modifier = 0.1
}

halfling_minority_integrated_large = {
	local_state_maintenance_modifier = 0.30		#they're so well integrated we cant even tell who's a criminal or not!
	local_unrest = 1
	local_autonomy = 0.1
	
	local_development_cost = -0.2
	province_trade_power_modifier = 0.15
}


#Humans - when oppressed they're just cannon fodder, then integrated they help develop provinces
human_minority_oppressed_small = {
	local_state_maintenance_modifier = 0.05
	local_unrest = 2							#Humans are not docile creatures
	local_autonomy = 0.05
	
	local_manpower_modifier = 0.1				#If you're gonna have humans in your province you're gonna draft them to your army cos they're so abundant and versatile
	#local_development_cost = -0.05
}

human_minority_oppressed_large = {
	local_state_maintenance_modifier = 0.1
	local_unrest = 3
	local_autonomy = 0.1
	
	local_manpower_modifier = 0.1				
	local_development_cost = -0.05				#There's a reason why humans have huge cities and are the most powerful
}

human_minority_coexisting_small = {
	local_state_maintenance_modifier = 0.15
	local_unrest = 2
	local_autonomy = 0.05
	
	local_manpower_modifier = 0.1				
	local_development_cost = -0.1
}

human_minority_coexisting_large = {
	local_state_maintenance_modifier = 0.2
	local_unrest = 3
	local_autonomy = 0.1
	
	local_manpower_modifier = 0.15			
	local_development_cost = -0.1
}

human_minority_integrated_small = {
	local_state_maintenance_modifier = 0.25		#Integrating humans is hard cos they're used to people integrating to their way stubborn
	local_unrest = 1
	local_autonomy = 0.05
	
	local_manpower_modifier = 0.15				
	local_development_cost = -0.15
}

human_minority_integrated_large = {
	local_state_maintenance_modifier = 0.30		
	local_unrest = 2
	local_autonomy = 0.1
	
	local_manpower_modifier = 0.2			
	local_development_cost = -0.15
}


#Half Orcish - when oppressed they're just thugs and guards, when integrated they defend provinces
half_orcish_minority_oppressed_small = {
	local_state_maintenance_modifier = 0.15
	local_unrest = 2
	local_autonomy = 0.05
	
	local_manpower_modifier = 0.1
	#local_defensiveness = 0.05
}

half_orcish_minority_oppressed_large = {
	local_state_maintenance_modifier = 0.2
	local_unrest = 3
	local_autonomy = 0.1
	
	local_manpower_modifier = 0.1
	local_defensiveness = 0.05
}

half_orcish_minority_coexisting_small = {
	local_state_maintenance_modifier = 0.2
	local_unrest = 2
	local_autonomy = 0.05
	
	local_manpower_modifier = 0.1
	local_defensiveness = 0.1
}

half_orcish_minority_coexisting_large = {
	local_state_maintenance_modifier = 0.25
	local_unrest = 3
	local_autonomy = 0.1
	
	local_manpower_modifier = 0.15
	local_defensiveness = 0.1
}

half_orcish_minority_integrated_small = {
	local_state_maintenance_modifier = 0.3
	local_unrest = 2
	local_autonomy = 0.05
	
	local_manpower_modifier = 0.15
	local_defensiveness = 0.1
}

half_orcish_minority_integrated_large = {		
	local_state_maintenance_modifier = 0.35
	local_unrest = 2
	local_autonomy = 0.1
	
	local_manpower_modifier = 0.2
	local_defensiveness = 0.2
}


#Monstrous Races - HARDER to integrate
#Orcish - when oppressed they're just cheap manual labour, when integrated they help boost armies
orcish_minority_oppressed_small = {
	local_state_maintenance_modifier = 0.2
	local_unrest = 3
	local_autonomy = 0.1
	
	local_build_cost = 0.25				#Cheap manual labour
	#local_manpower_modifier = 0.1
}

orcish_minority_oppressed_large = {
	local_state_maintenance_modifier = 0.25
	local_unrest = 4
	local_autonomy = 0.2
	
	local_build_cost = 0.35
	local_manpower_modifier = 0.1		#Forcibly draft orcs into the army
}

orcish_minority_coexisting_small = {
	local_state_maintenance_modifier = 0.35
	local_unrest = 2
	local_autonomy = 0.1
	
	local_build_cost = 0.25
	local_manpower_modifier = 0.1
}

orcish_minority_coexisting_large = {
	local_state_maintenance_modifier = 0.40
	local_unrest = 3
	local_autonomy = 0.2
	
	local_build_cost = 0.35
	local_manpower_modifier = 0.15
}

orcish_minority_integrated_small = {
	local_state_maintenance_modifier = 0.45	
	local_unrest = 2
	local_autonomy = 0.1
	
	local_build_cost = 0.35
	local_manpower_modifier = 0.15
}

orcish_minority_integrated_large = {
	local_state_maintenance_modifier = 0.50	
	local_unrest = 2
	local_autonomy = 0.2
	
	local_build_cost = 0.5
	local_manpower_modifier = 0.2
}


#Kobold - when oppressed they're just cheap manual labour, when integrated they help harass enemies
kobold_minority_oppressed_small = {
	local_state_maintenance_modifier = 0.15
	local_unrest = 3
	local_autonomy = 0.1
	
	local_build_cost = 0.1				#Cheap manual labour
	#local_hostile_attrition = 1
}

kobold_minority_oppressed_large = {
	local_state_maintenance_modifier = 0.2
	local_unrest = 4
	local_autonomy = 0.2
	
	local_build_cost = 0.2				
	#local_hostile_attrition = 1
}

kobold_minority_coexisting_small = {
	local_state_maintenance_modifier = 0.3
	local_unrest = 2
	local_autonomy = 0.1
	
	local_build_cost = 0.2				
	local_hostile_attrition = 1		#Kobolds can now coexist and can defend provinces
}

kobold_minority_coexisting_large = {
	local_state_maintenance_modifier = 0.35
	local_unrest = 3
	local_autonomy = 0.2
	
	local_build_cost = 0.25			
	local_hostile_attrition = 1	
}

kobold_minority_integrated_small = {
	local_state_maintenance_modifier = 0.35
	local_unrest = 2
	local_autonomy = 0.1
	
	local_build_cost = 0.25			
	local_hostile_attrition = 2		
}

kobold_minority_integrated_large = {
	local_state_maintenance_modifier = 0.4
	local_unrest = 2
	local_autonomy = 0.2
	
	local_build_cost = 0.3		
	local_hostile_attrition = 2	
}

kobold_majority_integrated_large = {
	local_state_maintenance_modifier = 0.4
	local_unrest = 2
	local_autonomy = 0.2
	
	local_culture_conversion_cost = -0.5 #Cos they're monsters they're easy to genocide?
	
	local_build_cost = 0.3		
	local_hostile_attrition = 2	
}

#Gnollish - when oppressed they're just cheap manual labour, when integrated they help harass enemies
gnollish_minority_oppressed_small = {
	local_state_maintenance_modifier = 0.2
	local_unrest = 3
	local_autonomy = 0.1
	
	local_manpower_modifier = 0.1		#Gnollish mercs
	#local_production_efficiency = 0.05
}

gnollish_minority_oppressed_large = {
	local_state_maintenance_modifier = 0.25
	local_unrest = 4
	local_autonomy = 0.2
	
	local_manpower_modifier = 0.1		
	local_production_efficiency = 0.05	#Scavengers help preserve goods
}

gnollish_minority_coexisting_small = {
	local_state_maintenance_modifier = 0.35
	local_unrest = 3
	local_autonomy = 0.1
	
	local_manpower_modifier = 0.1		
	local_production_efficiency = 0.1
}

gnollish_minority_coexisting_large = {
	local_state_maintenance_modifier = 0.4
	local_unrest = 4
	local_autonomy = 0.2
	
	local_manpower_modifier = 0.15	
	local_production_efficiency = 0.1
}

gnollish_minority_integrated_small = {
	local_state_maintenance_modifier = 0.45
	local_unrest = 2
	local_autonomy = 0.1
	
	local_manpower_modifier = 0.1	
	local_production_efficiency = 0.15
}

gnollish_minority_integrated_large = {
	local_state_maintenance_modifier = 0.5
	local_unrest = 3
	local_autonomy = 0.2
	
	local_manpower_modifier = 0.15	
	local_production_efficiency = 0.15
}


magical_minority = {
}

#Special Racial Province Modifiers

