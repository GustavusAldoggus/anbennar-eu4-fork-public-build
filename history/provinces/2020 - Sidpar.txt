# No previous file for Sidpar
owner = H05
controller = H05
add_core = H05
culture = caamas
religion = eordellon
capital = "Calcip"
fort_15th = yes

hre = no

base_tax = 2
base_production = 1
base_manpower = 1

trade_goods = chinaware

native_size = 14
native_ferocity = 6
native_hostileness = 6