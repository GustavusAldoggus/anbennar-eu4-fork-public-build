#173 - Labourd | 

owner = A26
controller = A26
culture = redscale_kobold
religion = kobold_dragon_cult

hre = no

base_tax = 3
base_production = 2
base_manpower = 2

trade_goods = wool
center_of_trade = 1

capital = ""

is_city = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_kobold

477.1.1 = {	
	owner = A26
	controller = A26
	add_core = A26
}