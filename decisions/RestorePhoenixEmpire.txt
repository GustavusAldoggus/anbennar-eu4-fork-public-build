country_decisions = {

	restore_phoenix_empire = {
		major = yes
		potential = {
			normal_or_historical_nations = yes
			NOT = { has_country_flag = restored_phoenix_empire_flag }
			OR = {
				ai = no
				is_playing_custom_nation = no
			}
			NOT = { tag = Z01 } #Anbennar
			NOT = { tag = F38 }
			NOT = { exists = F38 }
			OR = {
				religion_group = bulwari
			}
			OR = {
				primary_culture = sun_elf
				ruler_culture = sun_elf
			}
		}
		provinces_to_highlight = {
			OR = {
				superregion = bulwar_superregion
			}
			NOT = { owned_by = ROOT }
		}
		allow = {
			# bulwar_superregion = {
				# type = all
				# owned_by = ROOT
			# }
			is_at_war = no
			is_free_or_tributary_trigger = yes
			is_nomad = no
			num_of_owned_provinces_with = {
				value = 100
				superregion = bulwar_superregion
			}
		}
		effect = {
			601 = {
				move_capital_effect = yes
			}
			change_tag = F38
			hidden_effect = {
				every_owned_province = {
					limit = {
						is_part_of_hre = yes
					}
					set_in_empire = no
				}
			}
			set_government_rank = 3
			add_prestige = 50
			#if = {
			#	limit = {
			#		is_part_of_hre = yes
			#		is_elector = no
			#	}
			#	every_owned_province = {
			#		limit = {
			#			is_part_of_hre = yes
			#		}
			#		set_in_empire = no
			#	}
			#}
			if = {
				limit = { has_custom_ideas = no }
				country_event = { id = ideagroups.1 } #Swap Ideas
			}
			add_country_modifier = {
				name = "centralization_modifier"
				duration = 7300
			}
			set_country_flag = restored_phoenix_empire_flag
		}
		ai_will_do = {
			factor = 1
		}
	}
	
}
