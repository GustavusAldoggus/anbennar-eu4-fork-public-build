# No previous file for Leichow
owner = H10
controller = H10
add_core = H10
culture = tuathak
religion = eordellon
capital = "Androscoggin"

hre = no

base_tax = 4
base_production = 4
base_manpower = 2

trade_goods = gems

native_size = 14
native_ferocity = 6
native_hostileness = 6