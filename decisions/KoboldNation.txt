country_decisions = {
	contribute_to_the_dragon_cult_hoard = {
	
		potential = {
			religion = kobold_dragon_cult
		}

		allow = {
			treasury = 333
		}
		
		effect = {
			add_treasury = -333
			
			add_prestige = 20
			add_stability = 1
			add_yearly_manpower = 2
			
			add_legitimacy = 10
			add_republican_tradition = 10
			add_devotion = 10
		}
	}

	kobold_nation = {
		major = yes
		potential = {
			normal_or_historical_nations = yes
			NOT = { has_country_flag = formed_kobildzan_flag }
			NOT = { exists = Z38 } 
			OR = {
				ai = no
				is_playing_custom_nation = no
			}
			culture_group = kobold
			OR = {
				ai = no
				AND = {
					ai = yes
					num_of_cities = 3
				}
			}
			is_colonial_nation = no
			OR = {
				is_former_colonial_nation = no
				AND = {
					is_former_colonial_nation = yes
					ai = no
				}
			}
		}
		allow = {
			adm_tech = 10
			is_free_or_tributary_trigger = yes
			is_nomad = no
			is_at_war = no
			
			OR = {
				owns = 173
				owns = 197
			}
			owns = 189
			
			if = {
				limit = {
					tag = A26
				}
				NOT = { exists = A27 } 
				NOT = { exists = A28 } 
			}
			else_if = {
				limit = {
					tag = A27
				}
				NOT = { exists = A26 } 
				NOT = { exists = A28 } 
			}
			else_if = {
				limit = {
					tag = A28
				}
				NOT = { exists = A26 } 
				NOT = { exists = A27 } 
			}
			else_if = {
				limit = {
					culture_group = kobold
				}
				NOT = { exists = A26 } 
				NOT = { exists = A27 } 
				NOT = { exists = A28 } 
			}
		}
		effect = {
			189 = {
				move_capital_effect = yes
				add_base_tax = 4
				add_base_production = 4
				add_base_manpower = 4
			}
			
			change_tag = Z38
			custom_tooltip = tooltip_kobildzani_culture_provinces
			hidden_effect = {
				every_owned_province = {
					limit = {
						culture_group = kobold
					}
					change_culture = kobildzani_kobold
				}
			}
			change_primary_culture = kobildzani_kobold
			
			remove_non_electors_emperors_from_empire_effect = yes
			
			if = {
				limit = {
					NOT = { government_rank = 3 }
				}
				set_government_rank = 3
			}
			
			dragon_coast_region = {
				limit = {
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = Z38
			}
			
			add_prestige = 25
			
			add_country_modifier = {
				name = "centralization_modifier"
				duration = 7300
			}
			
			set_country_flag = formed_kobildzan_flag
		}
		
		ai_will_do = {
			factor = 1
		}
		
		ai_importance = 400
	}
}






















