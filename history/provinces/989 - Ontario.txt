# No previous file for Ontario
owner = Z19
controller = Z19
add_core = Z19
culture = gray_orc
religion = great_dookan
capital = ""


hre = no

base_tax = 1
base_production = 1
base_manpower = 1

trade_goods = unknown

native_size = 14
native_ferocity = 8
native_hostileness = 8