#319 -

owner = A48
controller = A48
add_core = A48
culture = wexonard
religion = regent_court

hre = yes

base_tax = 2
base_production = 2
base_manpower = 2

trade_goods = wool

capital = ""

is_city = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnollish
discovered_by = tech_gnomish
discovered_by = tech_harpy
discovered_by = tech_goblin
discovered_by = tech_bulwari

