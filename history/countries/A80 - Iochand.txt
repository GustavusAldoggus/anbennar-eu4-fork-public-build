government = monarchy
add_government_reform = autocracy_reform
government_rank = 2
primary_culture = creek_gnome
add_accepted_culture = iochander
religion = regent_court
technology_group = tech_gnomish
capital = 142 #Iochand
national_focus = DIP

1000.1.1 = { set_country_flag = mage_organization_decentralized_flag }