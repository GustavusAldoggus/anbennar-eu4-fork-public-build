# No previous file for Katmandu
owner = F21
controller = F21
add_core = F21
culture = bahari
religion = bulwari_sun_cult

base_tax = 2
base_production = 3
base_manpower = 2

trade_goods = iron

capital = ""

is_city = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnollish
discovered_by = tech_gnomish
discovered_by = tech_harpy
discovered_by = tech_goblin
discovered_by = tech_bulwari