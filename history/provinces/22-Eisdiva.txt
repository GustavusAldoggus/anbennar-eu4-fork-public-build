#Oplandene, incl. Hamar

owner = A88
controller = A88
add_core = A88
culture = west_damerian
religion = regent_court
hre = yes
base_tax = 2
base_production = 3
trade_goods = livestock
base_manpower = 3
capital = ""
is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish