# No previous file for Ust-Sysolsk
owner = H13
controller = H13
add_core = H13
culture = tuathak
religion = eordellon
capital = "Androscoggin"

hre = no

base_tax = 2
base_production = 1
base_manpower = 1

trade_goods = fur

native_size = 14
native_ferocity = 6
native_hostileness = 6
culture = peitar