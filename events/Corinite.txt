namespace = corinite

# The Corinite Reformation
country_event = {
	id = corinite.1
	title = corinite.1.t
	desc = corinite.1.d
	picture = RELIGION_eventPicture
	
	major = yes
	
	trigger = {
		NOT = { is_religion_enabled = corinite }
		religion = regent_court
		is_defender_of_faith = no	
		NOT = { has_reform = papacy_reform }
	#	reform_desire = 0.95
		#is_year = 1550
		#is age of greentide
		has_global_flag = "castellos_is_dead"
		capital_scope = { superregion = escann_superregion }
		NOT = { region = alenic_reach_region }
		any_owned_province = {
			can_have_center_of_reformation_trigger = {
				RELIGION = corinite
			}
		}
	}
	
	mean_time_to_happen = {
		months = 625 #625
		
		modifier = {
			factor = 0.4
			tag = B02 #Corintar
		}
		
		modifier = {
			factor = 0.5
			has_personal_deity = corin
		}
		
		modifier = {
			factor = 0.3
			OR = {
				capital_scope = { region = west_castanor_region}
				capital_scope = { region = south_castanor_region}
				capital_scope = { region = inner_castanor_region}
			}
			NOT = {
				OR = {
					culture_group = orcish
					culture_group = goblinoid
				}
			}
		}
		
		modifier = {
			factor = 0.6
			OR = {
				primary_culture = arbarani
				primary_culture = east_damerian
				primary_culture = gawedi
				primary_culture = vernman
				culture_group = escanni
			}
		}
		
		modifier = {
			factor = 0.75
			NOT = { num_of_cities = 10 }
		}
		# modifier = {
			# factor = 0.75
			# NOT = { num_of_cities = 2 }
		# }
		
		modifier = {
			factor = 0.75
			has_idea_group = innovativeness_ideas
		}
		modifier = {
			factor = 1.25
			has_idea_group = religious_ideas
		}
		modifier = {
			factor = 0.5
			num_of_rebel_controlled_provinces = 1
		}
		modifier = {
			factor = 2.0
			government = theocracy
		}
		modifier = {
			factor = 5
			OR = {
				tag = A74 #Nathalaire
				tag = B09 #House of Riches
				tag = B49 #Araionn
			}
		}
		modifier = {
			factor = 8
			OR = {
				culture_group = lencori	#cos they love Adean
				primary_culture = crownsman	#status quo was good for Ara money
			}
		}
		modifier = {
			factor = 10
			has_personal_deity = adean
		}
	}

	option = {
		name = "corinite.1.a"
		enable_religion = corinite
		random_owned_province = {
			limit = {
				can_have_center_of_reformation_trigger = {
					RELIGION = corinite
				}
			}
			change_religion = corinite
			add_reform_center = corinite
			add_permanent_province_modifier = {
				name = "religious_zeal_at_conv"
				duration = 9000
			}
		}
		capital_scope = {
			change_religion = corinite
			add_permanent_province_modifier = {
				name = "religious_zeal_at_conv"
				duration = 9000
			}
		}
		set_country_flag = "corinite_reformation" 
		
		hidden_effect = {
			every_country = {
				limit = {
					capital_scope = { continent = europe }
				}
				country_event = { id = corinite.2 days = 7300 }	#240 months aka 20 years
			}
		}
	}
}

# The Crimson Deluge
country_event = {
	id = corinite.2
	title = corinite.2.t
	desc = corinite.2.d
	picture = COMET_SIGHTED_eventPicture
	
	is_triggered_only = yes
	
	trigger = {
		is_religion_enabled = corinite
		capital_scope = {
			OR = {
				region = alenic_frontier_region
				region = forlorn_vale_region
				region = esmaria_region
				region = east_dameshead_region
				region = the_borders_region
				superregion = escann_superregion
			}
			NOT = { region = alenic_reach_region }
		}
		# any_owned_province = {
			# can_have_center_of_reformation_trigger = {
				# RELIGION = corinite
			# }
		# }
	}
	
	mean_time_to_happen = {
		days = 1
		
	}
	
	immediate = {
		set_global_flag = crimson_deluge
	}

	#Rejection
	option = {
		name = "corinite.2.a"
		ai_chance = {
			factor = 70
			modifier = {
				factor = 10
				has_personal_deity = adean
			}
			modifier = {
				factor = 5
				has_personal_deity = ara	#Adean's wife
			}
			modifier = {
				factor = 1.5
				ruler_has_personality = calm_personality
			}
			modifier = {
				factor = 1.5
				ruler_has_personality = careful_personality
			}
			modifier = {
				factor = 2
				ruler_has_personality = zealot_personality
			}
			modifier = {
				factor = 5
				OR = {
					tag = A74 #Nathalaire
					tag = B09 #House of Riches
					tag = B49 #Araionn
				}
			}
			modifier = {
				factor = 1.5
				OR = {
					capital_scope = { region = lencenor_region }
					capital_scope = { region = west_dameshead_region }
					capital_scope = { region = small_country_region }
					capital_scope = { region = dragon_coast_region }
					capital_scope = { region = damescrown_region }
					capital_scope = { region = esmaria_region }
				}
			}
		}
		add_stability = -2
	}
	
	#Accepting
	option = {
		name = "corinite.2.b"
		trigger = {
			religion = regent_court
		}
		ai_chance = {
			factor = 30
			modifier = {
				factor = 10
				has_personal_deity = corin
			}
			modifier = {
				factor = 100
				tag = B02
			}
			modifier = {
				factor = 5
				OR = {
					tag = B33	#Blademarches
					tag = B35	#Ancardia
					tag = A45	#Istralore
				}
			}
			modifier = {
				factor = 0
				government = theocracy
				NOT = { has_reform = magocracy_reform }
			}
			modifier = {
				factor = 0
				is_emperor = yes
			}
			modifier = {
				factor = 3
				OR = {
					capital_scope = { region = the_borders_region }			#heaviest rains here and also promoted HRE League War
					capital_scope = { region = east_dameshead_region }
				}
			}
			modifier = {
				factor = 3
				OR = {
					capital_scope = { region = west_castanor_region }
					capital_scope = { region = inner_castanor_region }
					capital_scope = { region = south_castanor_region }
				}
				OR = {
					NOT = { culture_group = orcish }
					NOT = { culture_group = goblinoid }
				}
			}
			modifier = {
				factor = 0.25
				OR = {
					culture_group = elven
					culture_group = gnomish
				}
			}
			modifier = {
				factor = 0.5
				OR = {
					culture_group = lencori
					primary_culture = crownsman
					primary_culture = esmari
				}
			}
			modifier = {
				factor = 1.3
				OR = {
					primary_culture = vernman	#Heroic cultures
					primary_culture = derannic
					primary_culture = arbarani
					culture_group = alenic
				}
			}
			modifier = {
				factor = 2
				personality = ai_militarist
			}
			modifier = {
				factor = 1.5
				ruler_has_personality = free_thinker_personality
			}
			modifier = {
				factor = 1.5
				ruler_has_personality = naive_personality
			}
			modifier = {
				factor = 2
				ruler_has_personality = sinner_personality
			}
			modifier = {
				factor = 2
				ruler_has_personality = tolerant_personality
			}
			modifier = {
				factor = 2
				ruler_has_personality = pious_personality
			}
		}
		change_religion = corinite
		random_owned_province = {
			limit = {
				can_have_center_of_reformation_trigger = {
					RELIGION = corinite
				}
			}
			change_religion = corinite
			add_reform_center = corinite
			add_permanent_province_modifier = {
				name = "religious_zeal_at_conv"
				duration = 9000
			}
		}
		capital_scope = {
			change_religion = corinite
			add_permanent_province_modifier = {
				name = "religious_zeal_at_conv"
				duration = 9000
			}
		}
		add_country_modifier = {
			name = "corinite_zeal"
			duration = 4500
		}
		add_stability = -1
		
		custom_tooltip = tooltip_remove_corin_personal_deity
	}
	option = {
		name = "corinite.2.c"
		trigger = {
			NOT = { religion_group = cannorian }
		}
		add_stability = -1
	}
	option = {
		name = "corinite.2.e"
		ai_chance = {
			factor = 1000
		}
		trigger = {
			religion = corinite
		}
		add_stability = 1
	}
}

# Pro-Corinite Magisters