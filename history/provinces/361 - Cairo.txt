#361 - Cairo

owner = A32
controller = A32
add_core = A32
culture = moon_elf
religion = elven_forebears

hre = no

base_tax = 6
base_production = 6
base_manpower = 3

trade_goods = grain

capital = ""

is_city = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_orcish
