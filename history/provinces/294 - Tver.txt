#294 - Tver

owner = Z02
controller = Z02
add_core = Z02
culture = wexonard
religion = regent_court

hre = yes

base_tax = 8
base_production = 4
base_manpower = 4

trade_goods = cloth
center_of_trade = 1

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnollish
discovered_by = tech_gnomish
