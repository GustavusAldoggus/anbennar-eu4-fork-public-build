#898 - Kansas

owner = A85
controller = A85
add_core = A85
culture = east_damerian
religion = regent_court

hre = yes

base_tax = 7
base_production = 4
base_manpower = 2

trade_goods = glass
center_of_trade = 1

capital = ""

is_city = yes
fort_15th = yes 

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
