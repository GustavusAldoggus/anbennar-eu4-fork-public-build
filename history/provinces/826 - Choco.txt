# No previous file for Choco

owner = B02
controller = B02
add_core = B02
culture = corintari
religion = regent_court

hre = no

base_tax = 2
base_production = 2
base_manpower = 3

trade_goods = grain

capital = ""

is_city = yes

native_size = 16
native_ferocity = 8
native_hostileness = 8


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_orcish