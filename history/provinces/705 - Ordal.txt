# No previous file for Ordal
owner = Z31
controller = Z31
add_core = Z31
culture = blue_reachman
religion = regent_court

hre = no

base_tax = 2
base_production = 2
base_manpower = 1

trade_goods = fish

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_gnomish
discovered_by = tech_kobold