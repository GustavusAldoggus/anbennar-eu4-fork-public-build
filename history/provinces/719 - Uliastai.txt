# No previous file for Uliastai
culture = gray_orc
religion = great_dookan
capital = ""
trade_goods = unknown

hre = no

base_tax = 2
base_production = 2
base_manpower = 2

native_size = 20
native_ferocity = 8
native_hostileness = 6