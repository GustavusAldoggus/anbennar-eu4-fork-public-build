
namespace = flavor_gawed

#Burning of the Redglades
country_event = {
	id = flavor_gawed.1
	title = flavor_gawed.1.t
	desc = flavor_gawed.1.d
	picture = BORDER_TENSION_eventPicture
	
	is_triggered_only = yes
	
	option = {
		name = flavor_gawed.1.a
		add_country_modifier = {
			name = "bane_of_elfkind"
			duration = -1
		}
		120 = {
			add_loot_from_rich_province_general_effect = {
				LOOTER = owner #BYZ
			}
			add_province_modifier = {
				name = "burning_of_the_redglades"
				duration = -1
			}
			change_culture = ROOT
		}
		115 = {
			add_loot_from_rich_province_general_effect = {
				LOOTER = owner #BYZ
			}
			add_province_modifier = {
				name = "burning_of_the_redglades"
				duration = -1
			}
			change_culture = ROOT 
		}
		116 = {
			add_loot_from_rich_province_general_effect = {
				LOOTER = owner #BYZ
			}
			add_province_modifier = {
				name = "burning_of_the_redglades"
				duration = -1
			}
			change_culture = ROOT 
		}
		117 = {
			add_loot_from_rich_province_general_effect = {
				LOOTER = owner #BYZ
			}
			add_province_modifier = {
				name = "burning_of_the_redglades"
				duration = -1
			}
			change_culture = ROOT 
		}
		118 = {
			add_loot_from_rich_province_general_effect = {
				LOOTER = owner #BYZ
			}
			add_province_modifier = {
				name = "burning_of_the_redglades"
				duration = -1
			}
			change_culture = ROOT 
		}
		
		hidden_effect = {
			every_country = {
				limit = {
					OR = {
						capital_scope = { continent = europe }
						capital_scope = { superregion = bulwar_superregion }
						capital_scope = { superregion = salahad_superregion }
					}
					NOT = { tag = ROOT }
				}
				country_event = { id = flavor_gawed.2 days = 7 }
			}
		}
	}

}

#Gawed burns the Redglades
country_event = {
	id = flavor_gawed.2
	title = flavor_gawed.2.t
	desc = flavor_gawed.2.d
	picture = BORDER_TENSION_eventPicture
	
	is_triggered_only = yes
	
	option = {	#This is preposterous!
		name = flavor_gawed.2.a
		trigger = {
			NOT = { culture_group = elven }
		}
		ai_chance = {
			factor = 80
			
			modifier = {
				factor = 1.5
				OR = {
					culture_group = lencori
					culture_group = anbennarian
					culture_group = escanni
					culture_group = businori
					is_rival = A13
					historical_rival_with = A13
				}
			}
		}
		add_opinion = { who = A13 modifier = A13_burned_the_redglades_anb }
	}
	
	option = {	#Elven reaction
		name = flavor_gawed.2.b
		trigger = {
			culture_group = elven
		}
		ai_chance = {
			factor = 100
			modifier = {
				factor = 100
				OR = {
					culture_group = elven
				}
			}
		}
		add_opinion = { who = A13 modifier = A13_burned_the_redglades_elf }
	}
	
	option = {	#Meh
		name = flavor_gawed.2.c
		ai_chance = {
			factor = 10
			
			modifier = {
				factor = 100
				OR = {
					alliance_with = A13
					historical_friend_with = A13
				}
			}
			modifier = {
				factor = 10
				OR = {
					culture_group = orcish
					culture_group = goblinoid
					culture_group = kobold
					culture_group = gerudian
					culture_group = divenori
					culture_group = alenic
				}
			}
			
		}
		
		if = {
			limit = { 
				OR = { 
					culture_group = elven
					culture_group = anbennarian	
					culture_group = lencori
					culture_group = escanni
					culture_group = businori
					culture_group = bulwari
				}
			}
			add_prestige = -10	#you're supposed to dislike em
		}
	}
}