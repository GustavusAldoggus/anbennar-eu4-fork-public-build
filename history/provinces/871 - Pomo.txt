# No previous file for Pomo
owner = B24
controller = B24
add_core = B24
culture = green_orc
religion = great_dookan
capital = ""

hre = no

base_tax = 1
base_production = 1
base_manpower = 2

trade_goods = iron

native_size = 35
native_ferocity = 7
native_hostileness = 5