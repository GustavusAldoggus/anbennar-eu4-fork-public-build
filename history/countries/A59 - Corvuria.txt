government = monarchy
add_government_reform = autocracy_reform
government_rank = 2
primary_culture = corvurian
religion = regent_court
technology_group = tech_cannorian
capital = 441 #Arca Corvur
national_focus = DIP

1000.1.1 = { set_country_flag = mage_organization_magisterium_flag }

1435.3.8 = {
	monarch = {
		name = "Davan II"
		dynasty = "s�l Vivin"
		birth_date = 1400.12.2
		adm = 2
		dip = 2
		mil = 4
	}
	heir = {
		name = "Camir"
		monarch_name = "Camir II"
		dynasty = "s�l Vivin"
		birth_date = 1424.3.1
		death_date = 1480.1.1
		claim = 100
		adm = 0
		dip = 5
		mil = 6
	}
	add_heir_personality = mage_personality
}