# No previous file for Burrholt
owner = Z08
controller = Z08
add_core = Z08
culture = east_dalr
religion = skaldhyrric_faith

hre = no

base_tax = 1
base_production = 2
base_manpower = 1

trade_goods = naval_supplies

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_gnomish
discovered_by = tech_giantkind
