#507 - Stargozdu (oldwood)

owner = F11
controller = F11
add_core = F11
add_core = F16
culture = exodus_goblin
religion = goblinic_shamanism


base_tax = 2
base_production = 2
base_manpower = 2

trade_goods = naval_supplies

capital = ""

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnollish
discovered_by = tech_gnomish
discovered_by = tech_harpy
discovered_by = tech_goblin
discovered_by = tech_bulwari

add_permanent_province_modifier = {
	name = human_minority_oppressed_small
	duration = -1
}
