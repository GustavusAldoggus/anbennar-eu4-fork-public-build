# No previous file for Musashi
owner = G01
controller = G01
add_core = G01
culture = boek
religion = totemism
capital = ""

hre = no

base_tax = 3
base_production = 4
base_manpower = 2

trade_goods = unknown

native_size = 10
native_ferocity = 5
native_hostileness = 3