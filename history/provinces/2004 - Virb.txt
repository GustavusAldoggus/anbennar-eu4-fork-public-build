# No previous file for Virb
owner = H01
controller = H01
add_core = H01
culture = selphereg
religion = eordellon
capital = "Virb"

hre = no

base_tax = 2
base_production = 3
base_manpower = 2

trade_goods = cotton

native_size = 14
native_ferocity = 6
native_hostileness = 6