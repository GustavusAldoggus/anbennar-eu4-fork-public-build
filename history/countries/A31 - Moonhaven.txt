government = republic
add_government_reform = long_lived_republic_reform
government_rank = 1
primary_culture = moon_elf
religion = regent_court
technology_group = tech_elven
capital = 53 # Moonhaven
national_focus = DIP
historical_friend = A45 #Istralore
historical_friend = A58 #Dameria

elector = yes

1000.1.1 = { set_country_flag = mage_organization_magisterium_flag }

1422.1.1 = { set_country_flag = lilac_wars_moon_party }

1439.2.4 = {
	monarch = {
		name = "Varion 'the Young'"	#This is the nephew of Varilor Bluetongue
		dynasty = "Bluetongue"
		birth_date = 1203.9.3
		adm = 6
		dip = 1
		mil = 4
	}
	add_ruler_personality = immortal_personality
	set_ruler_flag = set_immortality
}