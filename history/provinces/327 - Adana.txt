# No previous file for Adana
owner = A98
controller = A98
add_core = A98
culture = east_damerian
religion = regent_court

hre = yes

base_tax = 7
base_production = 6
base_manpower = 5

trade_goods = paper

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish