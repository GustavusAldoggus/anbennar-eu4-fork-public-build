#652 - Sedovcipastin (Grey sheep pasture)

owner = F13
controller = F13
add_core = F13
add_core = F22
culture = exodus_goblin
religion = goblinic_shamanism


base_tax = 1
base_production = 3
base_manpower = 2

trade_goods = wool

capital = ""

is_city = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnollish
discovered_by = tech_gnomish
discovered_by = tech_harpy
discovered_by = tech_goblin
discovered_by = tech_bulwari