# No previous file for Komi
owner = H15
controller = H15
add_core = H15
culture = snecboth
religion = eordellon
capital = "Androscoggin"

hre = no

base_tax = 2
base_production = 2
base_manpower = 1

trade_goods = fish

native_size = 14
native_ferocity = 6
native_hostileness = 6