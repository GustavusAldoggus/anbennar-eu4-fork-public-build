estate_artificers_disaster = {
	potential = {
		has_estate = estate_artificers
		estate_influence = {
			estate = estate_artificers
			influence = 80
		}
	}


	can_start = {
		has_any_disaster = no
		estate_influence = {
			estate = estate_artificers
			influence = 100
		}
	}
	
	can_stop = {
		OR = {
			has_any_disaster = yes
			NOT = {
				estate_influence = {
					estate = estate_artificers
					influence = 100
				}		
			}
		}
	}
	
	down_progress = {
		factor = 1
	}
	
	progress = {
		modifier = {
			factor = 5
			estate_influence = {
				estate = estate_artificers
				influence = 100
			}
		}
	}
	
	can_end = {
		custom_trigger_tooltip = {
			tooltip = EST_CRUSHED_ARTIFICERS
			OR = {
				NOT = { has_country_flag = artificers_estate_in_power }
				has_country_flag = noble_estate_in_power
				has_country_flag = church_estate_in_power
				has_country_flag = burghers_estate_in_power
			}						
		}
	}
	
	modifier = {	
		artillery_cost = 1
		production_efficiency  = -0.5
		build_cost = 0.5
	}

	on_start = estate_disasters.11
	on_end = estate_disasters.12
	
	on_monthly = {
	}
}

