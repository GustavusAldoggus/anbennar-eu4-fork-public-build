# No previous file for Shenandoah
owner = H12
controller = H12
add_core = H12
culture = selphereg
religion = eordellon
capital = "Androscoggin"

hre = no

base_tax = 2
base_production = 2
base_manpower = 2

trade_goods = wool

native_size = 14
native_ferocity = 6
native_hostileness = 6