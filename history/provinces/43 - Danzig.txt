#43 - Danzig | Pearlsedge

owner = A11
controller = A11
add_core = A11
culture = pearlsedger
religion = regent_court

hre = yes

base_tax = 5
base_production = 3
base_manpower = 3

trade_goods = fish

capital = ""

is_city = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
