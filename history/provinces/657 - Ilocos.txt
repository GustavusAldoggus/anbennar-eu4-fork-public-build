#657 - Magair-ten-mar

owner = F22
controller = F22
add_core = F22
add_core = F21
culture = bahari
religion = bulwari_sun_cult

base_tax = 3
base_production = 3
base_manpower = 3

trade_goods = spices

capital = ""

is_city = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnollish
discovered_by = tech_gnomish
discovered_by = tech_harpy
discovered_by = tech_goblin
discovered_by = tech_bulwari
