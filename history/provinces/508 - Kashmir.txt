#508 - Landsend

owner = F12
controller = F12
add_core = F12
add_core = F16
culture = ourdi
religion = regent_court


base_tax = 1
base_production = 3
base_manpower = 2

trade_goods = fur
capital = ""

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnollish
discovered_by = tech_gnomish
discovered_by = tech_harpy
discovered_by = tech_goblin
discovered_by = tech_bulwari